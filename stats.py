#Calculate the mean and STD of values in a text file
#formatted as one floating point value per line
#
#Modified by Joe 15/2/13 

import sys
import math
values = []
for line in open(sys.argv[1]):
 value = float(line)
 values.append(value)

total = sum(values)
average = total/len(values)

diffSquared = []
for value in values:
 diff = value - average
 diffSquared.append(diff**2)


stdDev = math.sqrt(sum(diffSquared)/(len(values)-1)) 

print len(values), 'values were read in'
print 'The sum of the input values is:', total
print 'The average of the imput values is:', average
print 'The STD of input is:', stdDev
